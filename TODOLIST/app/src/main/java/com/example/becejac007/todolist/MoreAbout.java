package com.example.becejac007.todolist;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by becejac007 on 10/23/2016.
 */
public class MoreAbout extends Activity {

    private ArrayList<String> tasks=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_description);

        //Get the bundle
        Bundle bundle = getIntent().getExtras();

        //Extract the data…
        String name = bundle.getString("stuff");

        TextView nameView = (TextView) findViewById(R.id.textView2);
        nameView.setText(name);

        String description = bundle.getString("stuff2");

        TextView descView = (TextView) findViewById(R.id.textView4);
        descView.setText(description);


    }

    public void doneClicked(View v){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String task = pref.getString("task", null);

        if (task == null)
            task = "{\"task\": []}";

        try {
            JSONObject taskJson = new JSONObject(task);
            JSONArray JsonList = taskJson.getJSONArray("task");
            ArrayList<String> results = new ArrayList<>();

            // read task from json string into string array
            for (int i = 0; i < JsonList.length(); i++)
                results.add(JsonList.getString(i));

            // add new task into list of tasks
            Bundle bundle = getIntent().getExtras();

            //Extract the data…
            String name = bundle.getString("stuff");

            for(int i=0;i<results.size();i++){
                if(results.get(i).equals(name)){
                    results.set(i+2,"done");
                }
            }


            // format new task array into json string
            StringBuffer sb = new StringBuffer();
            sb.append("{\"task\":[");
            for (int i = 0; i < results.size(); i++) {
                sb.append("\"");
                sb.append(results.get(i));
                sb.append("\"");
                if (i != results.size() - 1)
                    sb.append(",");
            }
            sb.append("]}");

            // store new task string
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("task", sb.toString());
            editor.apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);

    }

    public void backClicked(View v){
        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);
    }

    public void deleteClicked(View v){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String task = pref.getString("task", null);

        if (task == null)
            task = "{\"task\": []}";

        try {
            JSONObject taskJson = new JSONObject(task);
            JSONArray JsonList = taskJson.getJSONArray("task");
            ArrayList<String> results = new ArrayList<>();

            // read tasks from json string into string array
            for (int i = 0; i < JsonList.length(); i++)
                results.add(JsonList.getString(i));

            // add new task into list of tasks
            Bundle bundle = getIntent().getExtras();

            //Extract the data…
            String name = bundle.getString("stuff");

            int br=0;
            for(int i=0;i<results.size();i++){
                if(results.get(i).equals(name)){
                    br=i;
                }
            }


            String b=""+results.size();
            results.remove(br+2);
            results.remove(br+1);
            results.remove(br);


            // format new task array into json string
            StringBuffer sb = new StringBuffer();
            sb.append("{\"task\":[");
            for (int i = 0; i < results.size(); i++) {
                sb.append("\"");
                sb.append(results.get(i));
                sb.append("\"");
                if (i != results.size() - 1)
                    sb.append(",");
            }
            sb.append("]}");

            // store new task string
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("task", sb.toString());
            editor.apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);


    }
}
