package com.example.becejac007.todolist;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ArrayList<String> tasks=new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private ListView taskListView;

    private String chosen,desc, flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        taskListView = (ListView) findViewById(R.id.to_do_list);

        updateUI();
    }

    public void addTask(View v){

        //Log.d("[addClicked]: ", "add Task");
        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.dialog, null);

        final EditText input1 = (EditText) textEntryView.findViewById(R.id.task);
        final EditText input2 = (EditText) textEntryView.findViewById(R.id.desc);


        input1.setText("", TextView.BufferType.EDITABLE);
        input2.setText("", TextView.BufferType.EDITABLE);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("TO DO LIST")
                .setView(textEntryView).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        //Log.i("AlertDialog","TextEntry 1 Entered "+input1.getText().toString());
                        //Log.i("AlertDialog","TextEntry 2 Entered "+input2.getText().toString());
                        saveTask(input1.getText().toString(),input2.getText().toString(),"undone");
                    }
                }).setNegativeButton("CANCEL",null);
        alert.show();
    }

    private void saveTask(String TaskName, String description, String done) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String task = pref.getString("task", null);

        if (task == null)
            task = "{\"task\": []}";

        try {
            JSONObject tasksJson = new JSONObject(task);
            JSONArray JsonList = tasksJson.getJSONArray("task");
            ArrayList<String> results = new ArrayList<>();

            // read tasks from json string into string array
            for (int i = 0; i < JsonList.length(); i++)
                results.add(JsonList.getString(i));

            // add new task into list of tasks
            String foo="";
            if(TaskName.equals(foo)) {
            }
            else{
                results.add(TaskName);
                if(description.equals(foo)){
                    results.add("no description");
                    results.add(done);
                }
                else{
                    results.add(description);
                    results.add(done);
                }
            }
            // format new task array into json string
            StringBuffer sb = new StringBuffer();
            sb.append("{\"task\":[");
            for (int i = 0; i < results.size(); i++) {
                sb.append("\"");
                sb.append(results.get(i));
                sb.append("\"");
                if (i != results.size() - 1)
                    sb.append(",");
            }
            sb.append("]}");

            // store new task string
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("task", sb.toString());
            editor.apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        updateUI();
    }

    public void moreClicked(View v){
        View parent = (View) v.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());

        try {
            JSONObject tasksJson = new JSONObject(PreferenceManager.getDefaultSharedPreferences(
                    this).getString("task", "No tasks!"));
            tasks.clear();
            JSONArray tasksArray = tasksJson.getJSONArray("task");
            for (int i = 0; i < tasksArray.length(); i++)
                tasks.add(tasksArray.getString(i));


        } catch (JSONException e) {
            Log.d("[Tasks]", e.toString());
        }
        for (int i = 0 ; i < tasks.size(); i++){
            String d=tasks.get(i)+" / done";
            String d2=tasks.get(i)+" / undone";
            if(d.equals(task) && i%3==0){
                chosen=tasks.get(i);
                desc=tasks.get(i+1);
                flag=tasks.get(i+2);
            }
            else if(d2.equals(task) && i%3==0){
                chosen=tasks.get(i);
                desc=tasks.get(i+1);
                flag=tasks.get(i+2);
            }
        }

        Intent i=new Intent(parent.getContext(),MoreAbout.class);

        String getWord=chosen;
        String getWord2=desc;
        String getWord3=flag;
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("stuff", getWord);
        bundle.putString("stuff2",getWord2);
        bundle.putString("stuff3",getWord3);
        //Add the bundle to the intent
        i.putExtras(bundle);
        startActivity(i);

        updateUI();
    }


    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    private void updateUI() {
        ArrayList<String> taskList = new ArrayList<>();

        try {
            JSONObject tasksJson = new JSONObject(PreferenceManager.getDefaultSharedPreferences(
                    this).getString("task", "No tasks!"));
            tasks.clear();
            JSONArray tasksArray = tasksJson.getJSONArray("task");
            for (int i = 0; i < tasksArray.length(); i++)
                tasks.add(tasksArray.getString(i));


        } catch (JSONException e) {
            Log.d("[Tasks]", e.toString());
        }


        for (int i = 0 ; i < tasks.size(); i++){
            if(i%3==0) {
                String d=tasks.get(i)+" / "+tasks.get(i+2);
                taskList.add(d);
            }
        }

        if (adapter == null) {
            adapter = new ArrayAdapter<>(this,
                    R.layout.to_do_item,
                    R.id.task_title,
                    taskList);
            taskListView.setAdapter(adapter);
        } else {
            adapter.clear();
            adapter.addAll(taskList);
            adapter.notifyDataSetChanged();
        }
    }

}
